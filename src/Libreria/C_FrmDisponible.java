/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Libreria;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Uriel_nlvlerd
 */
public class C_FrmDisponible implements ActionListener {
    //DefaultListModel lista=new DefaultListModel();
    private M_FrmDisponible modelo;
    private V_FrmDisponible vista;
   
    public C_FrmDisponible(M_FrmDisponible modelo,V_FrmDisponible vista){
         this.modelo=modelo;
         this.vista=vista;
         
         this.vista.registrar.addActionListener(this);
         this.vista.cancelar.addActionListener(this);
         this.vista.VerRegistro.addActionListener(this);
     }
    public void iniciarVista(){
    vista.setTitle("Libreria");
    vista.pack();
    vista.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    vista.setLocationRelativeTo(null);
    vista.setVisible(true);  
    }
    
    public void actionPerformed(ActionEvent evento){
         /*if (vista.cancelar == evento.getSource()){
                   vista.cp1.getText();
                   vista.cp2.getText();
                   vista.cp3.getText();
           vista.cp1.setText("");
           vista.cp2.setText("");
           vista.cp3.setText("");*/
         if(vista.registrar == evento.getSource()){
          if(vista.cp1.getText().contentEquals("")){
           JOptionPane.showMessageDialog(null,"Ingrese el Nombre del libro");
           }else if(vista.cp2.getText().contentEquals("")){
           JOptionPane.showMessageDialog(null,"Ingrese el Autor del libro");
           }else if(vista.cp3.getText().contentEquals("")){
               JOptionPane.showMessageDialog(null,"Ingrese la categoria del libro");
           } else{
           vista.table1.addRow(new Object[]{vista.cp1.getText(),vista.cp2.getText(),vista.cp3.getText()});
           vista.cp1.setText("");
           vista.cp2.setText("");
           vista.cp3.setText("");
           JOptionPane.showMessageDialog(null,"Guardado Exitoso");
           }
          vista.table2.setEnabled(false);
          
        }
        if(vista.VerRegistro == evento.getSource()){
         vista.t.setVisible(true);
        }if (vista.cancelar == evento.getSource()){
                         /*vista.cp1.getText();
                         vista.cp2.getText();
                         vista.cp3.getText();
            vista.cp1.setText("");
            vista.cp2.setText("");
            vista.cp3.setText("");*/
            modelo.setNombre(vista.cp1.getText());
            modelo.setAutor(vista.cp2.getText());
            modelo.setCategoria(vista.cp3.getText());
            vista.cp1.setText(modelo.getNombre());
            vista.cp2.setText(modelo.getAutor());    
            vista.cp3.setText(modelo.getCategoria());
       }
        }
        
    }


