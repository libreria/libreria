/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Libreria;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Uriel_nlvlerd
 */

public class C_FrmDevoluciones implements ActionListener { 
    private  M_FrmDevoluciones modelo;
    private  V_FrmDevoluciones vista;
    private  String Serie;
    public C_FrmDevoluciones(M_FrmDevoluciones modelo,V_FrmDevoluciones vista){
         this.modelo=modelo;
         this.vista=vista;
         
         this.vista.Registrar.addActionListener(this);
         this.vista.Cancelar.addActionListener(this);
         this.vista.VerRegistro.addActionListener(this);
    }
    
    
    //DefaultTableModel table1 = new DefaultTableModel();
    //JTable table2 = new JTable(table1);
    
    public void iniciarVista(){
    vista.setTitle("Libreria");
    vista.pack();
    vista.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    vista.setLocationRelativeTo(null);
    vista.setVisible(true);  
    }
       
     public void actionPerformed(ActionEvent evento){
        if(vista.Registrar == evento.getSource()){
          if(vista.cp1.getText().contentEquals("")){
           JOptionPane.showMessageDialog(null,"Ingrese el codigo del libro");
           }else if(vista.cp2.getText().contentEquals("")){
           JOptionPane.showMessageDialog(null,"Ingrese el Titulo del libro");
           }else{
          vista.table1.addRow(new Object[]{vista.cp1.getText(),vista.cp2.getText(),});
           vista.cp1.setText("");
           vista.cp2.setText("");
           JOptionPane.showMessageDialog(null,"Guardado Exitoso");
           vista.table2.setEnabled(false);
           }
           /*table2.setEnabled(false);*/
        }
        if(vista.VerRegistro == evento.getSource()){
         vista.t.setVisible(true);
        }if (vista.Cancelar == evento.getSource()){
                         //vista.cp1.getText();
                         //vista.cp2.getText();
            modelo.setSerie(vista.cp1.getText());
            modelo.setNombre(vista.cp2.getText());
            vista.cp1.setText(modelo.getSerie());
            vista.cp2.setText(modelo.getNombre());
            
                
           
       }
      }
   
        /*En esta parte construiremos las columnas que tendra nuestra tabla
        en la que vamos a trabajar*/
        /*public void actionPerformed(ActionEvent evento){
        final JFrame t=new JFrame();
        t.setSize(350,350);
        t.setLocation(350,350);
        t.add(new JScrollPane(table2));
        t.setVisible(false);
        table1.addColumn("Codigo");
        table1.addColumn("Titulo del libro");
        //Si el usuario teclea el boton Registrar
         vista.Registrar.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e){
           if(vista.cp1.getText().contentEquals("")){
           JOptionPane.showMessageDialog(null,"Ingrese el codigo del libro");
           }else if(vista.cp2.getText().contentEquals("")){
           JOptionPane.showMessageDialog(null,"Ingrese el Titulo del libro");
           }else{
           table1.addRow(new Object[]{vista.cp1.getText(),vista.cp2.getText(),});
           vista.cp1.setText("");
           vista.cp2.setText("");
           JOptionPane.showMessageDialog(null,"Guardado Exitoso");
           }
         }
        
        });
         //Cierra la operacion que pasara cuando el usuario oprima el boton Registrar
        vista.VerRegistro.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
        t.setVisible(true);
            }
               
           });
    }*///Cierra actionPerformed
}//Llave final


