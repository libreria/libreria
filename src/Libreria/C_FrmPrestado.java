/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Libreria;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Uriel_nlvlerd
 */
public class C_FrmPrestado implements ActionListener {
    
    private M_FrmPrestado modelo;
    private V_FrmPrestado vista;
   
    public C_FrmPrestado(M_FrmPrestado modelo,V_FrmPrestado vista){
         this.modelo=modelo;
         this.vista=vista;
         
         this.vista.Registrar.addActionListener(this);
         this.vista.Cancelar.addActionListener(this);
         this.vista.VerRegistro.addActionListener(this);
     }
    public void iniciarVista(){
    vista.setTitle("Libreria");
    vista.pack();
    vista.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    vista.setLocationRelativeTo(null);
    vista.setVisible(true);  
    }
    /*public void actionPerfomed(ActionEvent evento){
        if(vista.Registrar == evento.getSource()){
               lista.addElement(vista.cp1.getText()+""+
                         vista.cp2.getText()+""+
                         vista.cp3.getText()+"");
                             modelo.setNombre("");
                             modelo.setAutor("");
                             modelo.setCategoria("");
                             vista.lista.setModel(lista); 
        }
        else{}
       
        
    }
    public void actionPerformed(ActionEvent evento){
     if(vista.Cancelar==evento.getSource()){
        modelo.setNombre("");
        modelo.setAutor("");
        modelo.setCategoria("");
        lista.removeAllElements();
        vista.lista.setModel(lista);
        }
     else{}
    }*/
    public void actionPerformed(ActionEvent evento){
    if(vista.Registrar == evento.getSource()){
          if(vista.cp1.getText().contentEquals("")){
           JOptionPane.showMessageDialog(null,"Ingrese el codigo del libro");
           }else if(vista.cp2.getText().contentEquals("")){
           JOptionPane.showMessageDialog(null,"Ingrese el Autor del libro");
           }else if(vista.cp3.getText().contentEquals("")){
           JOptionPane.showMessageDialog(null,"Ingrese la Categoria del libro");
           } else if(vista.cp4.getText().contentEquals("")){
           JOptionPane.showMessageDialog(null,"Ingrese la Editorial del libro");
           }else{
           vista.table1.addRow(new Object[]{vista.cp1.getText(),vista.cp2.getText(),vista.cp3.getText(),vista.cp4.getText()});
           vista.cp1.setText("");
           vista.cp2.setText("");
           vista.cp3.setText("");
           vista.cp4.setText("");
           JOptionPane.showMessageDialog(null,"Guardado Exitoso");
           vista.table2.setEnabled(false);
           }
    }
           /*table2.setEnabled(false);*/
        
        if(vista.VerRegistro == evento.getSource()){
         vista.t.setVisible(true);
        }if (vista.Cancelar == evento.getSource()){
                         /*vista.cp1.getText();
                         vista.cp2.getText();
                         vista.cp3.getText();
                         vista.cp4.getText();
            //modelo.setSerie(vista.cp1);
            //modelo.setNombre(vista.cp2);
            vista.cp1.setText("");
            vista.cp2.setText("");
            vista.cp3.setText("");
            vista.cp4.setText("");*/
            modelo.setCodigo(vista.cp1.getText());
            modelo.setAutor(vista.cp2.getText());
            modelo.setCategoria(vista.cp3.getText());
            modelo.setEditorial(vista.cp4.getText());
            vista.cp1.setText(modelo.getCodigo());
            vista.cp2.setText(modelo.getAutor());    
            vista.cp3.setText(modelo.getCategoria());
            vista.cp4.setText(modelo.getEditorial());
        }
    }
        
}

