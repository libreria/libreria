
package Libreria;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Uriel
 */
public class V_FrmDevoluciones extends JFrame {
    public JFrame t=new JFrame();
    public JButton Registrar,Cancelar,VerRegistro;
    public JLabel  Principal,Codigo,Titulo;
    public JTextField cp1,cp2;
    public DefaultTableModel table1 = new DefaultTableModel();
    public JTable table2 = new JTable(table1);
    public V_FrmDevoluciones(){
       //Se muestra la ventana creada
    JPanel panelMayor = new JPanel(new BorderLayout());
    JPanel panelNorte = new JPanel();
    JPanel panelCentro = new JPanel();
    JPanel panelSur= new JPanel();
    //Layout Managger a utilizar     
   FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER);
   panelNorte.setLayout(fl1);
   
   GridBagLayout gbl1 = new GridBagLayout();
   GridBagConstraints gbc1 = new GridBagConstraints();
   panelCentro.setLayout(gbl1);
   
   FlowLayout fl2 = new FlowLayout();
   panelSur.setLayout(fl2);
        
    //Panel Norte    
   JLabel titulo = new JLabel("Registro de Libros Devueltos");
   //Panel centro
   Codigo = new JLabel("Codigo del libro:");
   cp1 = new JTextField(25);
   Titulo= new JLabel("Titulo del libro");
   cp2 = new JTextField(25);
   //Panel Sur
   Registrar = new JButton("Guardar");
   Cancelar  = new JButton("Cancelar");
   VerRegistro=new JButton("Libros Registrados");
      
    //Agregacion de componentes al panel Norte    
    panelNorte.add(titulo);
    
    //Agregacion de componentes al panel Centro
        gbc1.gridx=0;
        gbc1.gridy=0;
        gbc1.anchor = GridBagConstraints.WEST;
        panelCentro.add(Codigo,gbc1);
        gbc1.gridy=1;
        gbc1.gridwidth=5;
        panelCentro.add(cp1,gbc1);
        gbc1.gridy=2;
        gbc1.gridwidth=1;
        panelCentro.add(Titulo,gbc1);
        gbc1.gridy=3;
        gbc1.gridwidth=5;
        panelCentro.add(cp2,gbc1);
       
    //Agregacion de componentes al panel Sur   
        panelSur.add(Registrar);
        panelSur.add(Cancelar);
        panelSur.add(VerRegistro);
        
      
    
    panelMayor.add(panelNorte,BorderLayout.NORTH);
    panelMayor.add(panelCentro,BorderLayout.CENTER);
    panelMayor.add(panelSur,BorderLayout.SOUTH);
    
    //Asociacion al JFrame
    this.add(panelMayor);
      //CONSTRUCCCION DE LA TABLA QUE CONTENDRA LOS VALORES REGISTRADOS
    table1.addColumn("Codigo");
    table1.addColumn("Titulo del libro");
    t.setSize(350,350);
    t.setLocation(350,350);
    t.add(new JScrollPane(table2));
    t.setVisible(false);
        
        /*En esta parte construiremos las columnas que tendra nuestra tabla
        en la que vamos a trabajar*/
       /* table1.addColumn("Codigo");
        table1.addColumn("Titulo del libro");
    
        final JFrame t=new JFrame();
        t.setSize(350,350);
        t.setLocation(350,350);
        t.add(new JScrollPane(table2));
        t.setVisible(false);
        
        
        Registrar.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
           if(cp1.getText().contentEquals("")){
           JOptionPane.showMessageDialog(null,"Ingrese el codigo del libro");
           }else if(cp2.getText().contentEquals("")){
           JOptionPane.showMessageDialog(null,"Ingrese el Titulo del libro");
           }else{
           table1.addRow(new Object[]{cp1.getText(),cp2.getText(),});
           cp1.setText("");
           cp2.setText("");
           JOptionPane.showMessageDialog(null,"Guardado Exitoso");
           }
           table2.setEnabled(false);
            
         }
        
        });
        VerRegistro.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
             t.setVisible(true);
        }
        });
        /*Cancelar.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
             cp1.setText("");
             cp2.setText("");
             
        }
        });*/
       
    
    
    
  }
 }
